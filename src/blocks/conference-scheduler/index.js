import Select, {createFilter} from 'react-select';

const { registerBlockType} = wp.blocks;
const { __, sprintf } = wp.i18n;
const { InspectorControls } = wp.editor;
const {
  PanelBody,
  ToggleControl,
  Spinner,
} = wp.components;
const { apiFetch } = wp;
const { withSelect } = wp.data;


const CSIcon = (
  <svg
    width={20}
    height={20}
    viewBox={"0 0 80 80"}
    className={"dashicon"}
  >
    <path
      d={"M35,2.5h10v20H35V2.5z M26.094,38.594c0-1.667-0.599-3.099-1.797-4.297S21.667,32.5,20,32.5s-3.099,0.599-4.297,1.797s-1.797,2.63-1.797,4.297s0.599,3.073,1.797,4.219s2.63,1.719,4.297,1.719s3.099-0.573,4.297-1.719S26.094,40.261,26.094,38.594z M30,60.312c0-3.75-0.963-6.901-2.891-9.453s-4.245-3.828-6.953-3.828s-5.026,1.276-6.953,3.828s-2.891,5.703-2.891,9.453c0,2.292,1.641,3.906,4.922,4.844s6.562,0.938,9.844,0S30,62.604,30,60.312z M37.5,57.5c-0.729,0-1.328,0.234-1.797,0.703S35,59.271,35,60s0.234,1.328,0.703,1.797S36.771,62.5,37.5,62.5h30.469c0.625,0,1.198-0.234,1.719-0.703s0.781-1.067,0.781-1.797s-0.26-1.328-0.781-1.797S68.594,57.5,67.969,57.5H37.5z M37.5,47.5c-0.729,0-1.328,0.234-1.797,0.703S35,49.271,35,50s0.234,1.328,0.703,1.797S36.771,52.5,37.5,52.5h30.469c0.625,0,1.198-0.234,1.719-0.703s0.781-1.067,0.781-1.797s-0.26-1.328-0.781-1.797S68.594,47.5,67.969,47.5H37.5z M37.5,37.5c-0.729,0-1.328,0.234-1.797,0.703S35,39.271,35,40s0.234,1.328,0.703,1.797S36.771,42.5,37.5,42.5h30.469c0.625,0,1.198-0.234,1.719-0.703s0.781-1.067,0.781-1.797s-0.26-1.328-0.781-1.797S68.594,37.5,67.969,37.5H37.5z M75,12.5c1.354,0,2.526,0.495,3.516,1.484S80,16.146,80,17.5v55c0,1.354-0.495,2.526-1.484,3.516S76.354,77.5,75,77.5H5c-1.354,0-2.526-0.495-3.516-1.484S0,73.854,0,72.5v-55c0-1.354,0.495-2.526,1.484-3.516S3.646,12.5,5,12.5h25v5c-1.354,0-2.526,0.495-3.516,1.484S25,21.146,25,22.5s0.495,2.526,1.484,3.516S28.646,27.5,30,27.5h5h10h5c1.354,0,2.526-0.495,3.516-1.484S55,23.854,55,22.5s-0.495-2.526-1.484-3.516S51.354,17.5,50,17.5v-5H75z" }
    />
  </svg>
);

registerBlockType('conf-scheduler/display',{
  title: __('Conference Scheduler'),
  description: __('Display your workshops.'),
  icon: CSIcon,
  category: 'widgets',
  keywords: [
    __('conference'),
    __('schedule'),
    __('workshops'),
  ],
  attributes: {
    displaySession: {
      type: 'number',
      default: ''
    },
    limitSessions: {
      type: 'number',
      default: 0
    },
    content: {
      type: 'string',
      default: ''
    },
    defaultState: {
      type: 'string',
      default: ''
    }
  },

  edit: withSelect( ( select ) => {
  		return {
  			sessions: select('core').getEntityRecords( 'taxonomy', 'conf_sessions' , { per_page: -1 })
  		};
  	} )( props => {
    const {
      attributes: { displaySession, limitSessions, content, defaultState },
      sessions,
      className,
      setAttributes,
    } = props;

    if ( ! sessions || ! sessions.length ) {
				return (
					<p className={className} >
            <Spinner />
						{ __( 'Loading workshops and session data', 'conf-scheduler' ) }
					</p>
				);
			}

    const setDisplaySession = newVal => {
			setAttributes( { displaySession: newVal.value, content: '' } );
		};

    const setDefaultState = newVal => {
			setAttributes( { defaultState: newVal.value } );
		};

    const setLimitSessions = checked => {
      const newVal = checked ? 1 : 0;
			setAttributes( { limitSessions: newVal , displaySession: '', content: '' } );
		};

    const getBlockInterface = (displaySessionId) => {
      var url = `/conference-scheduler/v1/get-block`;
      if (displaySessionId) url += '/' + displaySessionId;
      return apiFetch( {path: url} )
        .then((json) => {
          setAttributes({ content: json.renderedSessions });
        });
    };

    // options for the Inspector Selects
    const stringify = option => option.label;
    const filterOption = createFilter({ ignoreCase: true, stringify });
    const sessionOptions = sessions.map(function(s){ return {value: s.id, label: s.name } });

    const defaultStateOptions = [
      {value: '', label: __('Days Only', 'conf-scheduler')},
      {value: 'parent_sessions', label: __('Top Level Sessions Only', 'conf-scheduler')},
      {value: 'all_sessions', label: __('All Sessions', 'conf-scheduler')},
      {value: 'open', label: __('All Sessions and Workshops', 'conf-scheduler')},
    ];

    // Fix react-select display issues in WP
    const customStyles = {
  		menu: ( styles ) => {
  			return { ...styles, position: 'relative' };
  		},
      input: ( base, state ) => ( {
  			...base,
  			'& input:focus': {
  				boxShadow: state.isFocused ? 'none !important' : 'none !important',
  			},
  		} ),
  	};


    function SesssionsList(props) {
      const { children } = props;
      const display = defaultState == '' ? children.filter(item => item.type == 'day') : children;

      if (display.length) return display.map((renderedSession) =>
        <div><h3 key={renderedSession.id}>{renderedSession.title}</h3>
          {renderedSession.children && renderedSession.children.length && ['all_sessions','open'].indexOf(defaultState) > -1 && <SesssionsList children={renderedSession.children}/>}
        </div>
      );
      return defaultState == 'open' ? <div class="workshop">Workshops</div> : '';
    };

    let displayedSessions = '';
    if (!content) {
      getBlockInterface(displaySession);
      displayedSessions = <p><Spinner />{ __( 'Loading workshops and session data', 'conf-scheduler' ) }</p>
    } else {
      displayedSessions = <SesssionsList children={content}/>
    }

    const editorBlock = (
      <div className={className}>
        {__('Search:', 'conf-scheduler')}<select><option value="">{__('All Themes', 'conf-scheduler')}</option></select>
        <select><option value="">{__('All Keywords', 'conf-scheduler')}</option></select>
        <input type="text" value={__('Search...', 'conf-scheduler')}/>
        <button type="button">{__('Show/Hide All', 'conf-scheduler')}</button>
        <button type="button">{__('Show My Picks', 'conf-scheduler')}</button>
        {displayedSessions}
      </div>
    );

    const controls = (
      <InspectorControls key="inspector">
        <PanelBody title={__('Conference Scheduler Settings', 'conf-scheduler')}>
          <ToggleControl
        		label={ __( 'Limit Session Display', 'conf-scheduler' ) }
        		checked={ !! limitSessions }
        		help={ ( checked ) => checked ? __( 'Limit Sessions', 'conf-scheduler' ) : __( 'Show all Sessions', 'conf-scheduler' ) }
        		onChange={ setLimitSessions }
        	/>
          { !! limitSessions &&
            <div>
              <p>{__('Display children of:', 'conf-scheduler')}</p>
              <p><Select
                options={sessionOptions}
                className="confSchedulerSessions"
                value={ sessionOptions.find(option => option.value === displaySession) }
    						onChange={ setDisplaySession }
                isSearchable
                filterOption={filterOption}
                styles={ customStyles }
                /></p>
            </div>
          }
          <div>
            <p>{__('Show on load:', 'conf-scheduler')}</p>
            <p><Select
              options={defaultStateOptions}
              defaultValue={defaultStateOptions[0]}
              className="confSchedulerDefaultState"
              value={ defaultStateOptions.find(option => option.value === defaultState)}
              onChange={ setDefaultState }
              filterOption={filterOption}
              styles={ customStyles }
              /></p>
          </div>
        </PanelBody>
      </InspectorControls>
    );

    return [ controls, editorBlock ];
  }),
  save( ) {
    return null;
  },
});
